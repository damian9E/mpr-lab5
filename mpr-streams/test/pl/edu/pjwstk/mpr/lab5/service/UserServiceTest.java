package pl.edu.pjwstk.mpr.lab5.service;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;
import pl.edu.pjwstk.mpr.lab5.domain.Person;
import pl.edu.pjwstk.mpr.lab5.domain.Role;
import pl.edu.pjwstk.mpr.lab5.domain.Address;
import pl.edu.pjwstk.mpr.lab5.domain.Permission;
import pl.edu.pjwstk.mpr.lab5.domain.User;


public class UserServiceTest {
	
	@Test
	public void testFindUsersWhoHaveMoreThanOneAddress() {
		System.out.println("findUsersWhoHaveMoreThanOneAddress");
		
		Person person1 = new Person();
		Person person2 = new Person();
		Person person3 = new Person();
		Person person4 = new Person();
		person1.setAddresses(Arrays.asList(new Address()));
		person2.setAddresses(null);
		person3.setAddresses(Arrays.asList(new Address(), new Address()));
		person4.setAddresses(Arrays.asList(new Address(), new Address(), new Address(), new Address()));
		
		User user1 = new User();
		User user2 = new User();
		User user3 = new User();
		User user4 = new User();
		user1.setPersonDetails(person1);
		user2.setPersonDetails(person2);
		user3.setPersonDetails(person3);
		user4.setPersonDetails(person4);
		
		assertEquals(
				Arrays.asList(user3, user4),
				UserService.findUsersWhoHaveMoreThanOneAddress(
						Arrays.asList(user1, user2, user3, user4)
				)
		);
	}

	
	@Test
	public void testFindOldestPerson() {
		System.out.println("findOldestPerson");
		
		Person person1 = new Person().setAge(17);
		Person person2 = new Person().setAge(32);
		Person person3 = new Person(); 
		
		User user1 = new User().setPersonDetails(person1);
		User user2 = new User().setPersonDetails(person2);
		User user3 = new User().setPersonDetails(person3);
		User user4 = new User(); 
		
		assertEquals(person2, UserService.findOldestPerson(Arrays.asList(
				user1,
				user2,
				user3,
				user4
		)));
	}

	
	@Test
	public void testFindUserWithLongestUsername() {
		System.out.println("findUserWithLongestUsername");
		
		User user1 = new User().setName("someone");
		User user2 = new User().setName("somebody");
		User user3 = new User().setName("noone");
		User user4 = new User().setName("nobody");
		User user5 = new User(); 
		
		assertEquals(user1, UserService.findUserWithLongestUsername(Arrays.asList(
				user1,
				user2,
				user3,
				user4,
				user5
		)));
	}

	
	@Test
	public void testGetNamesAndSurnamesCommaSeparatedOfAllUsersAbove18() {
		System.out.println("getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18");
		
		Person person1 = new Person().setName("Julia"); 
		Person person2 = new Person().setName("Weronika").setSurname("Rosati");
		Person person3 = new Person().setName("Andrzej").setSurname("Konkol");
		Person person4 = new Person(); 	
		User user1 = new User().setPersonDetails(person1);
		User user2 = new User().setPersonDetails(person2);
		User user3 = new User().setPersonDetails(person3);
		User user4 = new User().setPersonDetails(person4);
		User user5 = new User(); 
		
		assertEquals("Weronika Rosati,Andrzej Konkol", UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(Arrays.asList(
				user1,
				user2,
				user3,
				user4,
				user5
		)));
	}

	
	@Test
	public void testGetSortedPermissionsOfUsersWithNameStartingWithA() {
		System.out.println("getSortedPermissionsOfUsersWithNameStartingWithA");
		
		Role role1 = new Role().setPermissions(Arrays.asList(
				new Permission().setName("Insert"),
				new Permission().setName("Delete"),
				new Permission().setName("Update"),
				new Permission().setName("Select")
		));
		Role role2 = new Role().setPermissions(Arrays.asList(
				new Permission().setName("Truncate"),
				new Permission().setName("Drop")
		));
		Role role3 = new Role();
		
		Person person1 = new Person().setRole(role1);
		Person person2 = new Person().setRole(role2);
		Person person3 = new Person().setRole(role3);
		Person person4 = new Person();
		
		User user1 = new User().setName("Admin").setPersonDetails(person1);
		User user2 = new User().setName("User").setPersonDetails(person2);
		User user3 = new User().setName("AdminWithoutPermissions").setPersonDetails(person3); 
		User user4 = new User().setName("UserWithoutRole").setPersonDetails(person4); 
		User user5 = new User().setName("AnotherUser"); 
		User user6 = new User(); 
		
		assertEquals(Arrays.asList(
				"Delete",
				"Insert",
				"Select",
				"Update"
		), UserService.getSortedPermissionsOfUsersWithNameStartingWithA(Arrays.asList(
				user1,
				user2,
				user3,
				user4,
				user5,
				user6
		)));
	}

	
	@Test
	public void testPrintCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS() {
		System.out.println("printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS");
		
		Role role1 = new Role().setPermissions(Arrays.asList(
				new Permission().setName("Insert"),
				new Permission().setName("Delete"),
				new Permission().setName("Update"),
				new Permission().setName("Select")
		));
		Role role2 = new Role().setPermissions(Arrays.asList(
				new Permission().setName("Truncate"),
				new Permission().setName("Drop")
		));
		Role role3 = new Role();
		
		Person person1 = new Person().setSurname("Sss").setRole(role1);
		Person person2 = new Person().setSurname("Sss").setRole(role2);
		Person person3 = new Person().setSurname("Sss").setRole(role3);
		Person person4 = new Person().setSurname("Sss");
		Person person5 = new Person();
		
		User user1 = new User().setPersonDetails(person1);
		User user2 = new User().setPersonDetails(person2);
		User user3 = new User().setPersonDetails(person3); 
		User user4 = new User().setPersonDetails(person4); 
		User user5 = new User().setPersonDetails(person5); 
		User user6 = new User(); 
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
		
		UserService.printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(Arrays.asList(
				user1,
				user2,
				user3,
				user4,
				user5,
				user6
		));
		
		
		System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
		
		assertEquals(
				String.join(
						System.lineSeparator(),
						new String [] {"INSERT", "DELETE", "UPDATE", "SELECT", "TRUNCATE", "DROP"}
				).trim(),
				out.toString().trim()
		);
	}

	
	@Test
	public void testGroupUsersByRole() {
		System.out.println("groupUsersByRole");
		
		Role role1 = new Role().setName("admin");
		Role role2 = new Role().setName("user");
		
		User user1 = new User().setPersonDetails(new Person().setRole(role1));
		User user2 = new User().setPersonDetails(new Person().setRole(role1));
		User user3 = new User().setPersonDetails(new Person().setRole(role2));
		User user4 = new User().setPersonDetails(new Person().setRole(role2));
		User user5 = new User().setPersonDetails(new Person().setRole(role2));
		User user6 = new User().setPersonDetails(new Person()); 
		User user7 = new User(); 
		
		Map<Role, List<User>> map = new HashMap();
		map.put(role1, Arrays.asList(user1, user2));
		map.put(role2, Arrays.asList(user3, user4, user5));
		
		assertEquals(map, UserService.groupUsersByRole(Arrays.asList(
				user1,
				user2,
				user3,
				user4,
				user5,
				user6,
				user7
		)));
	}

	
	@Test
	public void testPartitionUserByUnderAndOver18() {
		System.out.println("partitionUserByUnderAndOver18");
		
		User user1 = new User().setPersonDetails(new Person().setAge(14));
		User user2 = new User().setPersonDetails(new Person().setAge(19));
		User user3 = new User().setPersonDetails(new Person().setAge(23));
		User user4 = new User().setPersonDetails(new Person().setAge(5));
		User user5 = new User().setPersonDetails(new Person()); 
		User user6 = new User(); 
		
		Map<Boolean, List<User>> map = new HashMap();
		map.put(true, Arrays.asList(user2, user3));
		map.put(false, Arrays.asList(user1, user4));
		
		assertEquals(map, UserService.partitionUserByUnderAndOver18(Arrays.asList(
				user1,
				user2,
				user3,
				user4,
				user5,
				user6
		)));
	}
	
}
